package main

import (
	"os"
	"shareforce-api/Config"
	"shareforce-api/Models"
	"shareforce-api/Routes"

	"github.com/jinzhu/gorm"
)

var err error

func main() {

	Config.DB, err = gorm.Open("mysql", Config.DbURL(Config.BuildDBConfig()))
	Config.DB.LogMode(true)

	Config.DBWH, err = gorm.Open("mysql", Config.DbURL(Config.BuildDBWHConfig()))
	Config.DBWH.LogMode(true)

	// Config.DB.DB().Ping()
	// Config.DB.DB().Stats()

	defer Config.DB.Close()

	Config.DB.AutoMigrate(&Models.Shop{})
	Config.DB.AutoMigrate(&Models.ShopSchedule{})
	Config.DB.AutoMigrate(&Models.UserAccount{})

	Config.DBWH.AutoMigrate(&Models.PaymentMethods{})
	Config.DBWH.AutoMigrate(&Models.PaymentMethodLang{})

	router := Routes.SetupRouter()

	port := os.Getenv("PORT")
	if port == "" {
		port = "8888"
	}
	router.Run(":" + port)

}
