package Config

import (
	"fmt"

	"github.com/jinzhu/gorm"
)

var DB *gorm.DB
var DBWH *gorm.DB

type DBConfig struct {
	Host     string
	Port     int
	User     string
	DBName   string
	Password string
}

func DbURL(dbConfig *DBConfig) string {
	return fmt.Sprintf(
		"%s:%s@tcp(%s:%d)/%s?charset=utf8&parseTime=True&loc=Local",
		dbConfig.User,
		dbConfig.Password,
		dbConfig.Host,
		dbConfig.Port,
		dbConfig.DBName,
	)
}

func BuildDBConfig() *DBConfig {
	dbConfig := DBConfig{
		Host:     "leeoong.com",
		Port:     3306,
		User:     "leeoongc_shareforce",
		Password: "qI9K4;K8wEXg",
		DBName:   "leeoongc_shareforce",
	}
	return &dbConfig
}

func BuildDBWHConfig() *DBConfig {
	dbConfig := DBConfig{
		Host:     "leeoong.com",
		Port:     3306,
		User:     "leeoongc_shareforce",
		Password: "qI9K4;K8wEXg",
		DBName:   "leeoongc_admin_shwarehouse",
	}
	return &dbConfig
}
