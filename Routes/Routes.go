package Routes

import (
	"shareforce-api/Controllers"
	"shareforce-api/Middlewares"
	"time"

	"net/http"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

func SetupRouter() *gin.Engine {

	authMiddleware := Middlewares.SetupJwt()

	router := gin.Default()

	router.Use(gin.Logger())
	router.Use(cors.New(cors.Config{
		AllowOrigins:     []string{"http://localhost", "http://localhost:3000", "http://intervision.in", "http://leeoong.com", "http://loiplus.com"},
		AllowMethods:     []string{"GET", "POST", "PUT", "DELETE", "PATCH"},
		AllowHeaders:     []string{"Origin", "Content-Length", "Content-Type", "User-Agent", "Referrer", "Host", "Token", "Authorization"},
		ExposeHeaders:    []string{"Content-Length"},
		AllowCredentials: true,
		AllowAllOrigins:  false,
		AllowOriginFunc:  func(origin string) bool { return true },
		MaxAge:           12 * time.Hour,
	}))

	// allow all origin but can't use with jwt
	// router.Use(cors.Default())

	router.LoadHTMLGlob("templates/*.tmpl.html")
	router.Static("/static", "static")

	router.GET("/", func(c *gin.Context) {
		c.HTML(http.StatusOK, "index.tmpl.html", nil)
	})

	router.POST("/v1/login", authMiddleware.LoginHandler)
	router.GET("/v1/refresh-token", authMiddleware.RefreshHandler)

	router.GET("/v1/refresh-token-test", func(c *gin.Context) {
		c.JSON(http.StatusUnauthorized, gin.H{
			"code":    http.StatusUnauthorized,
			"message": "TEST",
		})
	})

	v1 := router.Group("/v1")
	v1.Use(authMiddleware.MiddlewareFunc())
	{

		v1.POST("/login-user", Middlewares.LoginUserHandler)

		v1.GET("users", Controllers.GetUsers)
		v1.GET("users/:id", Controllers.GetUserByID)
		v1.POST("users", Controllers.CreateUser)
		v1.PUT("users/:id", Controllers.UpdateUser)
		v1.DELETE("users/:id", Controllers.DeleteUser)

		v1.GET("shops/:id", Controllers.GetShopByID)
		v1.POST("shops", Controllers.CreateShop)
		v1.PUT("shops/:id", Controllers.UpdateShop)

		v1.GET("shop-schedules", Controllers.GetShopSchedules)
		v1.GET("shop-schedules/:id", Controllers.GetShopScheduleByID)
		v1.POST("shop-schedules", Controllers.CreateShopSchedule)
		v1.PUT("shop-schedules/:id", Controllers.UpdateShopSchedule)
		v1.DELETE("shop-schedules/:id", Controllers.DeleteShopSchedule)

		v1.GET("products", Controllers.GetProducts)
		v1.GET("product-lang/:product-id", Controllers.GetProductLang)

		v1.GET("payment-methods", Controllers.GetPaymentMethods)
		v1.POST("payment-methods", Controllers.CreatePaymentMethods)
		v1.PUT("payment-methods/:id", Controllers.UpdatePaymentMethods)
		v1.DELETE("payment-methods/:id", Controllers.DeletePaymentMethods)
		v1.GET("payment-method-lang/:payment-method-id", Controllers.GetPaymentMethodLang)

	}

	return router

}
