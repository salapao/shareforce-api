package Middlewares

import (
	"fmt"
	"log"
	"net/http"
	"shareforce-api/Config"
	"shareforce-api/Models"
	"time"

	jwt "github.com/appleboy/gin-jwt/v2"
	"github.com/gin-gonic/gin"
)

type login struct {
	Username string `form:"username" json:"username" binding:"required"`
	Password string `form:"password" json:"password" binding:"required"`
}

var identityKey = "username"

type User struct {
	Username string
}

func SetupJwt() *jwt.GinJWTMiddleware {

	authMiddleware, err := jwt.New(&jwt.GinJWTMiddleware{
		Realm:       "Shareforce API",
		Key:         []byte("NterVision@2020"),
		Timeout:     time.Hour,
		MaxRefresh:  time.Hour,
		IdentityKey: identityKey,
		PayloadFunc: func(data interface{}) jwt.MapClaims {

			if v, ok := data.(*User); ok {

				MapClaims := jwt.MapClaims{
					identityKey: v.Username,
				}

				// fmt.Println(" jwt.MapClaims : ", MapClaims)

				return MapClaims

			}

			return jwt.MapClaims{}

		},
		IdentityHandler: func(c *gin.Context) interface{} {
			claims := jwt.ExtractClaims(c)
			return &User{
				Username: claims[identityKey].(string),
			}
		},
		Authenticator: func(c *gin.Context) (interface{}, error) {

			var loginVals login
			if err := c.ShouldBind(&loginVals); err != nil {
				return "", jwt.ErrMissingLoginValues
			}

			var UserAccount Models.UserAccount
			Config.DB.DB().Ping()
			err := Config.DB.Unscoped().Where("username = ? && password = ?", loginVals.Username, loginVals.Password).First(&UserAccount).Error
			if err != nil {

				return nil, jwt.ErrFailedAuthentication

			} else {

				fmt.Println("Authenticator")
				fmt.Println("Logged in User_account_id : ", UserAccount.User_account_id)

				UserAccount.Last_login = time.Now()
				Config.DB.Unscoped().Save(&UserAccount)

				// อยากเพิ่มข้อมูลอะไรใน Token String ก็มาเพิ่มตรงนี้
				return &User{
					Username: loginVals.Username,
				}, nil

			}

		},
		Authorizator: func(data interface{}, c *gin.Context) bool {

			claims := jwt.ExtractClaims(c)

			// fmt.Printf(" \n \033[32m ======== Authorizator ======== \n ")
			// fmt.Println(claims[identityKey])
			// fmt.Printf(" \n \033[32m ======== Authorizator End ======== \n ")

			if v, ok := data.(*User); ok && v.Username == claims[identityKey] {
				return true
			}

			return false

		},
		Unauthorized: func(c *gin.Context, code int, message string) {
			c.JSON(code, gin.H{
				"code":    code,
				"message": message,
			})
		},
		// TokenLookup is a string in the form of "<source>:<name>" that is used
		// to extract token from the request.
		// Optional. Default value "header:Authorization".
		// Possible values:
		// - "header:<name>"
		// - "query:<name>"
		// - "cookie:<name>"
		// - "param:<name>"
		TokenLookup: "header: Authorization, query: token, cookie: jwt",
		// TokenLookup: "query:token",
		// TokenLookup: "cookie:token",

		// TokenHeadName is a string in the header. Default value is "Bearer"
		TokenHeadName: "Bearer",

		// TimeFunc provides the current time. You can override it to use another time value. This is useful for testing or if your server uses a different time zone than your tokens.
		TimeFunc: time.Now,
	})

	if err != nil {
		log.Fatal("JWT Error:" + err.Error())
	}

	return authMiddleware

}

func LoginUserHandler(c *gin.Context) {

	Authorization := c.Request.Header["Authorization"]

	claims := jwt.ExtractClaims(c)
	user, _ := c.Get(identityKey)

	var UserAccount Models.UserAccount

	fmt.Println(" loginUserHandler ")
	fmt.Println(" userID : ", claims[identityKey]) // ใช้ username เป็น id
	fmt.Println(" userName : ", user.(*User).Username)
	fmt.Println(" claims : ", claims)
	fmt.Println(" Authorization : ", Authorization[0])

	err := Config.DB.Unscoped().Where("Username = ?", user.(*User).Username).First(&UserAccount).Error
	if err != nil {
		c.AbortWithStatus(http.StatusNotFound)
	} else {

		UserAccount.Token = Authorization[0]
		Config.DB.Unscoped().Save(&UserAccount)

		var rs = make(map[string]string)
		rs["username"] = UserAccount.Username
		rs["name"] = UserAccount.First_name + " " + UserAccount.Last_name
		rs["fname"] = UserAccount.First_name
		rs["lname"] = UserAccount.Last_name

		c.JSON(http.StatusOK, gin.H{"user": rs})

	}

}
