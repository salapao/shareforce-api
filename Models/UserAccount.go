package Models

import "time"

type UserAccount struct {
	User_account_id            int64  `gorm:"primary_key;auto_increasment;not null"`
	Owner_account_id           int    `gorm:"not null"`
	Position_user_id           int    `gorm:"not null"`
	Gender_id                  int    `gorm:"not null"`
	Name_prefix_id             int    `gorm:"not null"`
	User_account_employee_code string `gorm:"type:varchar(100)"`
	User_account_code_sale     string `gorm:"type:varchar(100)"`
	Username                   string `gorm:"not null"`
	Password                   string `gorm:"not null"`
	Remember_token             string
	First_name                 string
	Last_name                  string
	Email                      string
	Pincode                    string `gorm:"type:varchar(45)"`
	Phone                      string `gorm:"type:varchar(45)"`
	Image                      string
	Status                     bool
	Token                      string
	Last_login                 time.Time
	Created_at                 time.Time
	Updated_at                 time.Time
	Deleted_at                 time.Time
}

func (b *UserAccount) TableName() string {
	return "user_account"
}
