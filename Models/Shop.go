package Models

import "mime/multipart"

type Shop struct {
	Shop_id             int64  `gorm:"primary_key;auto_increasment;not null"`
	Name                string `form:"Name"`
	Link                string `form:"Link"`
	Language            string `form:"Language"`
	Timezone            string `form:"Timezone"`
	Currency            string `form:"Currency"`
	LoginLogo           *multipart.FileHeader
	LoginLogoName       string `form:"LoginLogoName"`
	LoginBackground     *multipart.FileHeader
	LoginBackgroundName string `form:"LoginBackgroundName"`
	BackendLogo         *multipart.FileHeader
	BackendLogoName     string `form:"BackendLogoName"`
	SalesChannels       string `form:"SalesChannels"`
	CostType            string `form:"CostType"`
	UseSalesOnEmpty     bool   `form:"UseSalesOnEmpty"`
	UseRounding         bool   `form:"UseRounding"`
	UseSchedule         bool   `form:"UseSchedule"`
	UseServiceCharge    bool   `form:"UseServiceCharge"`
	UseTaxSystem        bool   `form:"UseTaxSystem"`
}

func (b *Shop) TableName() string {
	return "shop"
}
