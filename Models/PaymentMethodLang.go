package Models

type PaymentMethodLang struct {
	Payment_method_lang_id int64 `gorm:"primary_key;auto_increasment;not null"`
	PaymentMethodId        int64
	LangCode               string
	Name                   string
}

func (b *PaymentMethodLang) TableName() string {
	return "payment_method_lang"
}
