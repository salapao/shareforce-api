package Models

import "time"

type ProductLang struct {
	Product_lang_id    int64 `gorm:"primary_key;auto_increasment;not null"`
	LanguageBaseId     int64
	ProductId          int64
	ProductLangName    string
	ProductLangDetails string
	ProductLangCode    string
	ProductLangType    bool
	ProductLangStatus  bool
	CreatedAt          time.Time
	UpdatedAt          time.Time
	DeletedAt          time.Time
}

func (b *ProductLang) TableName() string {
	return "product_lang"
}
