package Models

import "time"

type Product struct {
	Product_id                int64 `gorm:"primary_key;auto_increasment;not null"`
	OwnerAccountId            int64
	UserAccountId             int64
	ProductUnitTypeId         int64
	BrandId                   int64
	ProductUnit               string
	ProductCounts             float64
	ProductAlertCoun          float64
	ProductCode               string
	ProductBarcode            string
	ProductQrcode             string
	ProductSerial             string
	ProductCostPrice          float64
	ProductDetailsweight      float64
	ProductWeightType         bool
	ProductWithholdingtaxType bool
	ProductStatus             bool
	CreatedAt                 time.Time
	UpdatedAt                 time.Time
	DeletedAt                 time.Time
}

func (b *Product) TableName() string {
	return "product"
}
