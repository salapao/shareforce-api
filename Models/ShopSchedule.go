package Models

type ShopSchedule struct {
	Schedule_id int64 `gorm:"primary_key;auto_increasment;not null"`
	Shop_id     int
	DayOfWeek   int
	OpenTime    int
	OpenHour    int
	OpenMinute  int
	CloseTime   int
	CloseHour   int
	CloseMinute int
}

func (b *ShopSchedule) TableName() string {
	return "shop_schedule"
}
