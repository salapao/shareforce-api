package Models

type PaymentMethods struct {
	Payment_method_id int64 `gorm:"primary_key;auto_increasment;not null"`
	IsActive          bool
}

func (b *PaymentMethods) TableName() string {
	return "payment_methods"
}
