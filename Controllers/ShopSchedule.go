package Controllers

import (
	"fmt"
	"net/http"
	"shareforce-api/Config"
	"shareforce-api/Models"

	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
)

func GetShopSchedules(c *gin.Context) {

	var ShopSchedule []Models.ShopSchedule

	fmt.Printf(" \033[31m ======== GetShopSchedules ======== \n ")

	Config.DB.DB().Ping()
	err := Config.DB.Order("day_of_week asc").Order("open_time asc").Find(&ShopSchedule).Error
	if err != nil {
		fmt.Printf(" \033[31m ======== Error : %v ======== \n ", err)
		c.AbortWithStatus(http.StatusNotFound)
	} else {
		c.JSON(http.StatusOK, ShopSchedule)
	}

}

func GetShopScheduleByID(c *gin.Context) {

	var ShopSchedule Models.ShopSchedule

	id := c.Params.ByName("id")
	Config.DB.DB().Ping()
	err := Config.DB.Unscoped().Where("schedule_id = ?", id).First(&ShopSchedule).Error
	if err != nil {
		fmt.Printf(" \033[31m ======== Error : %v ======== \n ", err)
		c.AbortWithStatus(http.StatusNotFound)
	} else {
		c.JSON(http.StatusOK, ShopSchedule)
	}

}

func CreateShopSchedule(c *gin.Context) {

	var ShopSchedule Models.ShopSchedule

	c.BindJSON(&ShopSchedule)
	fmt.Printf(" \033[33m ======== Struct : %#v ======== \n ", ShopSchedule)

	ShopSchedule.Schedule_id = 0

	ShopSchedule.OpenTime = (ShopSchedule.OpenHour * 60 * 60) + (ShopSchedule.OpenHour * 60)
	ShopSchedule.CloseTime = (ShopSchedule.CloseHour * 60 * 60) + (ShopSchedule.CloseHour * 60)

	Config.DB.DB().Ping()
	err := Config.DB.Unscoped().Save(&ShopSchedule).Error
	if err != nil {
		fmt.Printf(" \033[31m ======== Error : %v ======== \n ", err)
		c.AbortWithStatus(http.StatusNotFound)
	} else {
		c.JSON(http.StatusOK, ShopSchedule)
	}

}

func UpdateShopSchedule(c *gin.Context) {

	var ShopSchedule Models.ShopSchedule

	Config.DB.DB().Ping()
	err := Config.DB.Unscoped().Find(&ShopSchedule).Error
	if err != nil {

		if gorm.IsRecordNotFoundError(err) {
			fmt.Printf(" \033[31m ======== Error : %v ======== \n ", err)
			CreateShopSchedule(c)
		} else {
			fmt.Printf(" \033[31m ======== Error : %v ======== \n ", err)
			c.JSON(http.StatusNotFound, ShopSchedule)
		}

	} else {

		c.BindJSON(&ShopSchedule)
		fmt.Printf(" \033[33m ======== Struct : %#v ======== \n ", ShopSchedule)

		Config.DB.DB().Ping()
		err := Config.DB.Unscoped().Save(&ShopSchedule).Error
		if err != nil {
			fmt.Printf(" \033[31m ======== Error : %v ======== \n ", err)
			c.AbortWithStatus(http.StatusNotFound)
		} else {
			c.JSON(http.StatusOK, ShopSchedule)
		}

	}

}

func DeleteShopSchedule(c *gin.Context) {

	var ShopSchedule Models.ShopSchedule

	id := c.Params.ByName("id")
	Config.DB.DB().Ping()
	err := Config.DB.Where("schedule_id = ?", id).Delete(&ShopSchedule).Error
	if err != nil {
		fmt.Printf(" \033[31m ======== Error : %v ======== \n ", err)
		c.AbortWithStatus(http.StatusNotFound)
	} else {
		c.JSON(http.StatusOK, gin.H{"id " + id: "is deleted"})
	}

}
