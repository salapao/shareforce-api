package Controllers

import (
	"fmt"
	"log"
	"net/http"
	"shareforce-api/Config"
	"shareforce-api/Models"

	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
)

func GetUsers(c *gin.Context) {

	var UserAccount []Models.UserAccount

	err := Config.DB.Unscoped().Find(&UserAccount).Error
	if err != nil {
		c.AbortWithStatus(http.StatusNotFound)
	} else {
		c.JSON(http.StatusOK, UserAccount)
	}

}

func GetUserByID(c *gin.Context) {

	var UserAccount Models.UserAccount

	id := c.Params.ByName("id")
	Config.DB.DB().Ping()
	err := Config.DB.Unscoped().Where("user_account_id = ?", id).First(&UserAccount).Error
	if err != nil {
		fmt.Printf(" \033[31m ======== Error : %v ======== \n ", err)
		c.AbortWithStatus(http.StatusNotFound)
	} else {
		c.JSON(http.StatusOK, UserAccount)
	}

}

func CreateUser(c *gin.Context) {

	var UserAccount Models.UserAccount

	fmt.Println(" === USER CREATE === ")

	c.BindJSON(&UserAccount)
	err := Config.DB.Create(&UserAccount).Error
	if err != nil {
		fmt.Println(err.Error())
		c.AbortWithStatus(http.StatusNotFound)
	} else {
		c.JSON(http.StatusOK, UserAccount)
	}

}

func UpdateUser(c *gin.Context) {

	var UserAccount Models.UserAccount

	fmt.Println(" === USER UPDATE === ")

	id := c.Params.ByName("id")
	err := Config.DB.Unscoped().Where("user_account_id = ?", id).First(&UserAccount).Error
	if err != nil {

		fmt.Println(" ERROR : ", err)
		c.JSON(http.StatusNotFound, UserAccount)

	} else {

		fmt.Println(" === UpdateUser === ")

		c.BindJSON(&UserAccount)

		log.Printf("%v\n", &UserAccount)

		// fmt.Println(&UserAccount)
		Config.DB.Unscoped().Save(&UserAccount)
		fmt.Println(" === USER UPDATE SAVE === ")
		err = nil
		if err != nil {
			c.AbortWithStatus(http.StatusNotFound)
		} else {
			c.JSON(http.StatusOK, UserAccount)
		}

	}

}

func DeleteUser(c *gin.Context) {

	var UserAccount Models.UserAccount

	id := c.Params.ByName("id")
	err := Config.DB.Unscoped().Where("user_account_id = ?", id).Delete(&UserAccount)
	if err != nil {
		c.AbortWithStatus(http.StatusNotFound)
	} else {
		c.JSON(http.StatusOK, gin.H{"id" + id: "is deleted"})
	}

}
