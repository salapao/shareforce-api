package Controllers

import (
	"fmt"
	"net/http"
	"shareforce-api/Config"
	"shareforce-api/Models"

	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
)

func GetPaymentMethods(c *gin.Context) {

	var PaymentMethods []Models.PaymentMethods

	var q = "select * from payment_methods where is_active = 1"

	Config.DBWH.DB().Ping()
	err := Config.DBWH.Unscoped().Raw(q).Scan(&PaymentMethods).Error
	if err != nil {
		fmt.Printf(" \033[31m ======== Error : %v ======== \n ", err)
		c.AbortWithStatus(http.StatusNotFound)
	} else {
		c.JSON(http.StatusOK, PaymentMethods)
	}

}

func CreatePaymentMethods(c *gin.Context) {

	var PaymentMethods Models.PaymentMethods

	c.BindJSON(&PaymentMethods)
	fmt.Printf(" \033[33m ======== Struct : %#v ======== \n ", PaymentMethods)

	Config.DBWH.DB().Ping()
	err := Config.DBWH.Unscoped().Save(&PaymentMethods).Error
	if err != nil {
		fmt.Printf(" \033[31m ======== Error : %v ======== \n ", err)
		c.AbortWithStatus(http.StatusNotFound)
	} else {
		c.JSON(http.StatusOK, PaymentMethods)
	}

}

func UpdatePaymentMethods(c *gin.Context) {

	var PaymentMethods Models.PaymentMethods

	Config.DBWH.DB().Ping()
	err := Config.DBWH.Unscoped().Find(&PaymentMethods).Error
	if err != nil {

		if gorm.IsRecordNotFoundError(err) {
			fmt.Printf(" \033[31m ======== Error : %v ======== \n ", err)
			CreatePaymentMethods(c)
		} else {
			fmt.Printf(" \033[31m ======== Error : %v ======== \n ", err)
			c.JSON(http.StatusNotFound, PaymentMethods)
		}

	} else {

		c.BindJSON(&PaymentMethods)
		fmt.Printf(" \033[33m ======== Struct : %#v ======== \n ", PaymentMethods)

		Config.DBWH.DB().Ping()
		err := Config.DBWH.Unscoped().Save(&PaymentMethods).Error
		if err != nil {
			fmt.Printf(" \033[31m ======== Error : %v ======== \n ", err)
			c.AbortWithStatus(http.StatusNotFound)
		} else {
			c.JSON(http.StatusOK, PaymentMethods)
		}

	}

}

func DeletePaymentMethods(c *gin.Context) {

	var PaymentMethods Models.PaymentMethods

	id := c.Params.ByName("id")
	Config.DBWH.DB().Ping()
	err := Config.DBWH.Where("payment_method_id = ?", id).Delete(&PaymentMethods).Error
	if err != nil {
		fmt.Printf(" \033[31m ======== Error : %v ======== \n ", err)
		c.AbortWithStatus(http.StatusNotFound)
	} else {
		c.JSON(http.StatusOK, gin.H{"id " + id: "is deleted"})
	}

}

func GetPaymentMethodLang(c *gin.Context) {

	var PaymentMethodLang []Models.PaymentMethodLang

	payment_method_id := c.Params.ByName("payment-method-id")

	var q = "select * from payment_method_lang where payment_method_id = '" + payment_method_id + "'"

	Config.DBWH.DB().Ping()
	err := Config.DBWH.Unscoped().Raw(q).Scan(&PaymentMethodLang).Error
	if err != nil {
		fmt.Printf(" \033[31m ======== Error : %v ======== \n ", err)
		c.AbortWithStatus(http.StatusNotFound)
	} else {
		c.JSON(http.StatusOK, PaymentMethodLang)
	}

}
