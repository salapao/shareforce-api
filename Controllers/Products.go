package Controllers

import (
	"fmt"
	"net/http"
	"shareforce-api/Config"
	"shareforce-api/Models"

	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
)

func GetProducts(c *gin.Context) {

	var Product []Models.Product

	var q = "select * from product"

	Config.DBWH.DB().Ping()
	err := Config.DBWH.Unscoped().Raw(q).Scan(&Product).Error
	if err != nil {
		fmt.Printf(" \033[31m ======== Error : %v ======== \n ", err)
		c.AbortWithStatus(http.StatusNotFound)
	} else {
		c.JSON(http.StatusOK, Product)
	}

}

func GetProductLang(c *gin.Context) {

	var ProductLang []Models.ProductLang

	product_id := c.Params.ByName("product-id")

	var q = "select * from product_lang where product_id = '" + product_id + "'"

	Config.DBWH.DB().Ping()
	err := Config.DBWH.Unscoped().Raw(q).Scan(&ProductLang).Error
	if err != nil {
		fmt.Printf(" \033[31m ======== Error : %v ======== \n ", err)
		c.AbortWithStatus(http.StatusNotFound)
	} else {
		c.JSON(http.StatusOK, ProductLang)
	}

}
