package Controllers

import (
	"fmt"
	"net/http"
	"path/filepath"
	"shareforce-api/Config"
	"shareforce-api/Models"

	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
)

func GetShopByID(c *gin.Context) {

	var Shop Models.Shop

	id := c.Params.ByName("id")
	Config.DB.DB().Ping()
	err := Config.DB.Unscoped().Where("shop_id = ?", id).First(&Shop).Error
	if err != nil {
		fmt.Printf(" ======== Error : %v ======== \n ", err)
		c.AbortWithStatus(http.StatusNotFound)
	} else {
		c.JSON(http.StatusOK, Shop)
	}

}

func CreateShop(c *gin.Context) {

	var Shop Models.Shop

	c.Bind(&Shop)
	Config.DB.DB().Ping()
	err := Config.DB.Create(&Shop).Error
	if err != nil {
		fmt.Printf(" ======== Error : %v ======== \n ", err)
		c.AbortWithStatus(http.StatusNotFound)
	} else {
		c.JSON(http.StatusOK, Shop)
	}

}

func UpdateShop(c *gin.Context) {

	var Shop Models.Shop

	id := c.Params.ByName("id")
	Config.DB.DB().Ping()
	err := Config.DB.Unscoped().Where("shop_id = ?", id).First(&Shop).Error
	if err != nil {

		if gorm.IsRecordNotFoundError(err) {
			fmt.Printf(" \033[31m ======== Error : %v ======== \n ", err)
			CreateShop(c)
		} else {
			fmt.Printf(" \033[31m ======== Error : %v ======== \n ", err)
			c.JSON(http.StatusNotFound, Shop)
		}

	} else {

		c.Bind(&Shop)
		fmt.Printf(" \033[33m ======== Struct : %#v ======== \n ", Shop)

		file, file_err := c.FormFile("LoginLogo")
		if file_err != nil {
			fmt.Printf(" \033[31m ======== Error : %v ======== \n ", file_err)
		} else {
			ext := filepath.Ext(file.Filename)
			err := c.SaveUploadedFile(file, "uploaded/login-logo"+ext)
			if err != nil {
				fmt.Printf(" \033[31m ======== Error : %v ======== \n ", err)
			}
		}

		file, file_err = c.FormFile("LoginBackground")
		if file_err != nil {
			fmt.Printf(" \033[31m ======== Error : %v ======== \n ", file_err)
		} else {
			ext := filepath.Ext(file.Filename)
			err := c.SaveUploadedFile(file, "uploaded/login-background"+ext)
			if err != nil {
				fmt.Printf(" \033[31m ======== Error : %v ======== \n ", err)
			}
		}

		file, file_err = c.FormFile("BackendLogo")
		if file_err != nil {
			fmt.Printf(" \033[31m ======== Error : %v ======== \n ", file_err)
		} else {
			ext := filepath.Ext(file.Filename)
			err := c.SaveUploadedFile(file, "uploaded/backend-logo"+ext)
			if err != nil {
				fmt.Printf(" \033[31m ======== Error : %v ======== \n ", err)
			}
		}

		Config.DB.DB().Ping()
		err := Config.DB.Unscoped().Save(&Shop).Error
		if err != nil {
			fmt.Printf(" \033[31m ======== Error : %v ======== \n ", err)
			c.AbortWithStatus(http.StatusNotFound)
		} else {
			fmt.Printf(" \033[32m ======== UpdateShop Finish ======== \n ")
			c.JSON(http.StatusOK, Shop)
		}

	}

}
